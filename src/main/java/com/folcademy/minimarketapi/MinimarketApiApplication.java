package com.folcademy.minimarketapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinimarketApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinimarketApiApplication.class, args);
	}

}
